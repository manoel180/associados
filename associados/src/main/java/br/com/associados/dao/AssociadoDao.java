package br.com.associados.dao;

import br.com.associados.model.Associado;

public interface AssociadoDao extends GenericDao<Associado>{

}
