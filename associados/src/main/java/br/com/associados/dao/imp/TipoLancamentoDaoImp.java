package br.com.associados.dao.imp;

import org.springframework.stereotype.Repository;

import br.com.associados.dao.TipoLancamentoDao;
import br.com.associados.model.TiposLancamento;

@Repository
public class TipoLancamentoDaoImp extends GenericDaoImp<TiposLancamento> implements TipoLancamentoDao {
	
}
