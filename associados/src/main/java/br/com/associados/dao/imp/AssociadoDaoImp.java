package br.com.associados.dao.imp;

import org.springframework.stereotype.Repository;

import br.com.associados.dao.AssociadoDao;
import br.com.associados.model.Associado;

@Repository
public class AssociadoDaoImp extends GenericDaoImp<Associado> implements AssociadoDao {
	
}
