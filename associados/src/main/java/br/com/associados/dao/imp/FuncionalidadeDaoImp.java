package br.com.associados.dao.imp;

import org.springframework.stereotype.Repository;

import br.com.associados.dao.FuncionalidadeDao;
import br.com.associados.model.Funcionalidade;

@Repository
public class FuncionalidadeDaoImp extends GenericDaoImp<Funcionalidade> implements FuncionalidadeDao {
	
}
