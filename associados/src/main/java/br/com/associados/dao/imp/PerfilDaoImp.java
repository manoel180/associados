package br.com.associados.dao.imp;

import org.springframework.stereotype.Repository;

import br.com.associados.dao.PerfilDao;
import br.com.associados.model.Perfil;

@Repository
public class PerfilDaoImp extends GenericDaoImp<Perfil> implements PerfilDao {
	
}
