package br.com.associados.dao.imp;

import org.springframework.stereotype.Repository;

import br.com.associados.dao.PlanoDao;
import br.com.associados.model.Plano;

@Repository
public class PlanoDaoImp extends GenericDaoImp<Plano> implements PlanoDao {
	
}
