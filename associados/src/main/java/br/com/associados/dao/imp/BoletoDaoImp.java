package br.com.associados.dao.imp;

import org.springframework.stereotype.Repository;

import br.com.associados.dao.BoletoDao;
import br.com.associados.model.Boleto;

@Repository
public class BoletoDaoImp extends GenericDaoImp<Boleto> implements BoletoDao {
	
}
