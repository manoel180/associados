package br.com.associados.dao.imp;

import org.springframework.stereotype.Repository;

import br.com.associados.dao.FuncaoEclesiasticaDao;
import br.com.associados.model.FuncoesEclesiastica;

@Repository
public class FuncaoEclesiasticaDaoImp extends GenericDaoImp<FuncoesEclesiastica> implements FuncaoEclesiasticaDao {
	
}
